// Copyright 2023 Guo Tingjin(ericple)

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
import HapAbility from "./ability";
import HapExtAbility from "./extAbility";
import requestPermission from "./requestPermissions";

class ModuleInfo {
    abilities?: HapAbility[] = [];
    extensionAbilities?: HapExtAbility[] = [];
    compileMode: string = '';
    deliveryWithInstall: boolean = false;
    dependencies: [] = [];
    description: string = '';
    descriptionId: number = 0;
    deviceTypes: string[] = [];
    installationFree: boolean = false;
    mainElement: string = '';
    name: string = '';
    pages: string = '';
    requestPermissions: requestPermission[] = [];
    type: string = '';
    virtualMachine: string = '';
}

export default ModuleInfo;